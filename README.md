# Amalgam

Collect, filter, and count timestamped events.

## Usage

Import:

```js
import EventStack from '@kckckc/amalgam'
const stack = new EventStack()
```

Add events:

```js
// with timestamp
stack.insert(1651627042430, { type: 'a', value: 123 })
// with current time
stack.add({ type: 'b', value: 456 })
```

Find or count events:

```js
// Most recent is returned first
stack.find()
/* =>
[
	{ timestamp: 50000, data: { value: 456 } },
	{ timestamp: 40000, data: { value: 123 } },
]
*/

stack.count() // => 2
```

Filter events:

```js
stack.count({ where: { type: { $eq: 'a' } } }) // => 1

stack.count({
	whereAll: {
		'dot.path.to.prop': {
			$eq: value,
			$lt: value
			$lte: value
			// + gt, gte, in, nin, neq
		}
	},
	whereSome: {
		// ...
	},
	where: (item) => item.value === 'hello' // custom filter function
	before: 1234567, // events before timestamp
	after: 1234567, // events after timestamp
	recent: 5000, // events in the last 5000 ms
	afterEvent: {
		// ... another filter, to stop once this item is reached ...
		whereAll: {...}
		// ...
	}
})
```

`afterEvent` example:

```js
stack.add({});
stack.add({});
stack.add({ type: 'reset' });
stack.add({});
stack.add({});
stack.count({ afterEvent: { where: (i) => i.type === 'reset' });
// => 2
```

Group events:

```js
stack.findGrouped({
	// ... other filter options ...
	groupBy: 'path.to.prop', // property to group by
	group: (item) => item.type, // custom group function
})

stack.countGrouped({ ... })
```

