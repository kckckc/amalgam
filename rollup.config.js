import typescript from 'rollup-plugin-typescript2';
import resolve from '@rollup/plugin-node-resolve';
import renameNodeModules from 'rollup-plugin-rename-node-modules';
import pkg from './package.json';

export default {
	input: 'src/index.ts',
	output: [
		{ file: pkg.main, format: 'cjs' },
		{
			dir: 'dist',
			format: 'es',
			preserveModules: true,
			preserveModulesRoot: 'src',
		},
	],
	external: [
		...Object.keys(pkg.dependencies || {}),
		...Object.keys(pkg.peerDependencies || {}),
	],
	plugins: [
		resolve(),
		typescript({
			typescript: require('typescript'),
			useTsconfigDeclarationDir: true,
		}),
		renameNodeModules('ext'),
	],
};
