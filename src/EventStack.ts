import lodashGet from 'lodash.get';
import sortedIndexBy from 'lodash.sortedindexby';
import { Comparable, Operator, satisfies } from './satisfies';

export interface Event {
	timestamp: number;
	data: any;
}

export interface EventFilterOptions {
	// Data filters
	where?: WhereFn;
	whereAll?: WhereOptions;
	whereSome?: WhereOptions;

	// Timestamp filters
	before?: number;
	after?: number;
	recent?: number;

	afterEvent?: EventFilterOptions;
}
export type WhereFn = (e: Event) => boolean;
export type WhereOptions = Record<string, WhereClause>;
export type WhereClause = {
	[key in Operator]?: Comparable;
};
export interface GroupOptions {
	groupBy?: string;
	group?: GroupFn;
}
export type GroupFn = (e: Event) => string;

const checkWhere = (item: Event, filter: EventFilterOptions): boolean => {
	if (filter.where) {
		return filter.where(item);
	}

	const whereOptions = (filter.whereAll || filter.whereSome) as WhereOptions;
	const props = Object.keys(whereOptions);

	for (let pIdx = 0; pIdx < props.length; pIdx++) {
		const propPath = props[pIdx];

		const left = lodashGet(item.data, propPath);

		const opTypes: Operator[] = Object.keys(
			whereOptions[propPath]
		) as Operator[];

		let opFailed = false;
		for (let oIdx = 0; oIdx < opTypes.length; oIdx++) {
			const op = opTypes[oIdx];

			const right = whereOptions[propPath][op]!;

			if (!satisfies(left, op, right)) {
				opFailed = true;
				break;
			}
		}

		if (opFailed && filter.whereAll) {
			return false;
		}

		if (!opFailed && filter.whereSome) {
			return true;
		}
	}

	if (filter.whereSome) return false;

	if (filter.whereAll) return true;

	throw new Error('Should be unreachable');
};

const hasWhereOptions = (filter: EventFilterOptions) => {
	const a = filter.where ? 1 : 0;
	const b = filter.whereAll ? 1 : 0;
	const c = filter.whereSome ? 1 : 0;
	if (a + b + c > 1) {
		throw new Error('Only one where option can be used at a time');
	}
	return a + b + c > 0;
};

const isMatch = (item: Event, now: number, filter?: EventFilterOptions) => {
	// Time filters
	if (filter?.before && item.timestamp >= filter?.before) {
		return false;
	}
	if (filter?.after && item.timestamp <= filter?.after) {
		return false;
	}
	if (filter?.recent && now - item.timestamp > filter.recent) {
		return false;
	}

	// Data filters
	if (filter && hasWhereOptions(filter) && !checkWhere(item, filter)) {
		return false;
	}

	return true;
};

export class EventStack {
	items: Event[];

	constructor() {
		this.items = [];
	}

	add(data: any) {
		this.insert(Date.now(), data);
	}

	insert(timestamp: number, data: any) {
		const newItem = { timestamp, data };
		const idx = sortedIndexBy(this.items, newItem, (x) => x.timestamp);
		this.items.splice(idx, 0, newItem);
	}

	find(filter?: EventFilterOptions): Event[] {
		return (
			this.findGrouped({
				...filter,
			}).main ?? []
		);
	}

	findGrouped(
		filter?: EventFilterOptions & GroupOptions
	): Record<string, Event[]> {
		if (!filter) {
			filter = {};
		}

		const found: Record<string, Event[]> = {};

		// Sanity check for filter
		if (filter) {
			hasWhereOptions(filter);
		}

		const now = Date.now();

		for (let i = this.items.length - 1; i >= 0; i--) {
			const item = this.items[i];

			if (filter?.afterEvent && isMatch(item, now, filter.afterEvent)) {
				break;
			}

			if (filter && !isMatch(item, now, filter)) {
				continue;
			}

			// Add to group
			let group = 'main';
			if (filter.groupBy) {
				group = lodashGet(item.data, filter.groupBy);
			}
			if (filter.group) {
				group = filter.group(item);
			}
			if (!found[group]) {
				found[group] = [];
			}
			found[group].push(item);
		}

		return found;
	}

	count(filter?: EventFilterOptions): number {
		return this.find(filter).length;
	}

	countGrouped(
		filter?: EventFilterOptions & GroupOptions
	): Record<string, number> {
		const found = this.findGrouped(filter);
		const res: Record<string, number> = {};
		Object.keys(found).forEach((k) => {
			res[k] = found[k].length;
		});
		return res;
	}
}
