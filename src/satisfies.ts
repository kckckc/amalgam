export type Operator =
	| '$eq'
	| '$neq'
	| '$lt'
	| '$lte'
	| '$gt'
	| '$gte'
	| '$in'
	| '$nin';

export type Comparable = number | string | boolean | Comparable[];

export class ValidationError extends Error {
	constructor(message: string) {
		super();
		this.message = message;
	}
}

const assertNum = (d: Comparable) => {
	if (typeof d !== 'number') {
		throw new ValidationError('Expected number');
	}
};
const assertArray = (d: Comparable) => {
	if (!Array.isArray(d)) {
		throw new ValidationError('Expected array');
	}
	return true;
};

export const satisfies = (
	left: Comparable,
	operator: Operator,
	right: Comparable
): boolean => {
	if (operator === '$eq') {
		return left === right;
	} else if (operator === '$neq') {
		return left !== right;
	} else if (operator === '$lt') {
		assertNum(left);
		assertNum(right);
		return left < right;
	} else if (operator === '$lte') {
		assertNum(left);
		assertNum(right);
		return left <= right;
	} else if (operator === '$gt') {
		assertNum(left);
		assertNum(right);
		return left > right;
	} else if (operator === '$gte') {
		assertNum(left);
		assertNum(right);
		return left >= right;
	} else if (operator === '$in') {
		assertArray(right);
		return (right as Comparable[]).includes(left);
	} else if (operator === '$nin') {
		assertArray(right);
		return !(right as Comparable[]).includes(left);
	}

	throw new Error('Unknown operator');
};
