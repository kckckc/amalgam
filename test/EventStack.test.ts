import { EventStack } from '../src/EventStack';
import { HOUR, MINUTE } from '../src/consts';

const sleep = (ms: number) => new Promise((r) => setTimeout(r, ms));

describe('EventStack', () => {
	it('should add events', () => {
		const stack = new EventStack();
		stack.add({
			name: 'something',
			val: 123,
		});
	});

	it('should find events', () => {
		const stack = new EventStack();
		stack.add({
			val: 123,
		});
		expect(stack.find().map((e) => e.data)).toStrictEqual([
			{
				val: 123,
			},
		]);
	});

	it('should find recent events first', async () => {
		const stack = new EventStack();
		stack.add({
			val: 1,
		});
		await sleep(0);
		stack.add({
			val: 2,
		});
		await sleep(0);
		stack.add({
			val: 3,
		});
		expect(stack.find().map((e) => e.data)[0]).toStrictEqual({
			val: 3,
		});
	});

	it('should find by a filter', () => {
		const stack = new EventStack();
		stack.add({
			val: 1,
		});
		stack.add({
			val: 2,
			other: 'abc',
		});
		stack.add({
			val: 3,
		});
		expect(stack.find({ whereAll: { val: { $eq: 2 } } })[0].data).toEqual({
			val: 2,
			other: 'abc',
		});
	});

	it('should count events', () => {
		const stack = new EventStack();
		expect(stack.count()).toBe(0);
		stack.add({
			name: 'something',
			val: 123,
		});
		stack.add({
			name: 'another',
			val: 456,
		});
		stack.add({
			name: 'yetanother',
			val: 789,
		});
		expect(stack.count()).toBe(3);
	});

	it('should count by a filter', () => {
		const stack = new EventStack();
		stack.add({
			val: 'p',
		});
		stack.add({
			val: 'q',
		});
		stack.add({
			val: 'p',
		});
		expect(stack.count({ whereAll: { val: { $eq: 'p' } } })).toEqual(2);
	});

	it('should filter by property equality', () => {
		const stack = new EventStack();
		stack.add({ some: 'value' });
		stack.add({ some: 'value' });
		stack.add({ some: 'othervalue' });
		expect(
			stack
				.find({
					whereAll: {
						some: { $eq: 'value' },
					},
				})
				.map((e) => e.data)
		).toStrictEqual([{ some: 'value' }, { some: 'value' }]);
		expect(
			stack.find({
				whereAll: {
					doesnt: { $eq: 'exist' },
				},
			})
		).toStrictEqual([]);
	});

	it('should filter by nested properties', () => {
		const stack = new EventStack();
		stack.add({ some: { deep: 'val' } });
		stack.add({ some: { deep: 'val2' } });
		stack.add({ some: { deep: 'val' } });
		expect(
			stack
				.find({
					whereAll: {
						'some.deep': { $eq: 'val2' },
					},
				})
				.map((e) => e.data)
		).toStrictEqual([{ some: { deep: 'val2' } }]);
	});

	it('should filter by comparison', () => {
		const stack = new EventStack();
		stack.add({ val: 10 });
		stack.add({ val: 15 });
		stack.add({ val: 50 });
		expect(
			stack.count({
				whereAll: {
					val: { $lt: 20 },
				},
			})
		).toBe(2);
	});

	it('should filter by multiple clauses with whereAll', () => {
		const stack = new EventStack();
		stack.add({ aa: 10, bb: 'qq' });
		stack.add({ aa: 15, bb: 'rr' });
		stack.add({ aa: 20, bb: 'qq' });
		stack.add({ aa: 25, bb: 'rr' });
		stack.add({ aa: 30, bb: 'qq' });
		stack.add({ aa: 35, bb: 'rr' });
		expect(
			stack.count({
				whereAll: {
					aa: { $lt: 27 },
					bb: { $eq: 'qq' },
				},
			})
		).toBe(2);
	});
	it('should filter by multiple clauses with whereSome', () => {
		const stack = new EventStack();
		stack.add({ aa: 10, bb: 'qq' });
		stack.add({ aa: 15, bb: 'rr' });
		stack.add({ aa: 20, bb: 'qq' });
		stack.add({ aa: 25, bb: 'rr' });
		stack.add({ aa: 30, bb: 'qq' });
		stack.add({ aa: 35, bb: 'rr' });
		expect(
			stack.count({
				whereSome: {
					aa: { $lt: 27 },
					bb: { $eq: 'qq' },
				},
			})
		).toBe(5);
	});

	it('should filter by composite clauses', () => {
		const stack = new EventStack();
		stack.add({ aa: 10, bb: 'qq' });
		stack.add({ aa: 15, bb: 'rr' });
		stack.add({ aa: 20, bb: 'qq' });
		stack.add({ aa: 25, bb: 'rr' });
		stack.add({ aa: 30, bb: 'qq' });
		stack.add({ aa: 35, bb: 'rr' });
		expect(
			stack.count({
				whereAll: {
					aa: { $gt: 13, $lt: 33 },
				},
			})
		).toBe(4);
	});

	it('should filter by where function', () => {
		const stack = new EventStack();
		stack.add({ val: 1 });
		stack.add({ val: 2 });
		stack.add({ val: 3 });
		stack.add({ val: 4 });
		stack.add({ val: 5 });
		stack.add({ val: 6 });
		expect(
			stack.count({
				where: (i) => i.data.val % 2 === 0,
			})
		).toBe(3);
	});

	it('should only allow one where filter', () => {
		const stack = new EventStack();
		expect(() =>
			stack.count({
				whereAll: {
					aa: { $eq: 123 },
				},
				whereSome: {
					aa: { $eq: 123 },
				},
			})
		).toThrow();
		expect(() =>
			stack.count({
				where: () => true,
				whereAll: {
					aa: { $eq: 123 },
				},
			})
		).toThrow();
		expect(() =>
			stack.count({
				where: () => true,
				whereSome: {
					aa: { $eq: 123 },
				},
			})
		).toThrow();
	});

	it('should insert with a timestamp', () => {
		const stack = new EventStack();
		stack.insert(1000, { val: 123 });
		expect(stack.find()[0].timestamp).toBe(1000);
	});

	it('should insert in order', () => {
		const stack = new EventStack();
		stack.add({ val: 123 });
		stack.insert(1000, { val: 456 });
		expect(stack.find()[0].data.val).toBe(123);
	});

	it('should filter before a timestamp', () => {
		const stack = new EventStack();
		stack.insert(1000, { val: 123 });
		stack.insert(1100, { val: 123 });
		stack.insert(1200, { val: 123 });
		stack.insert(1300, { val: 123 });
		stack.insert(1400, { val: 123 });
		stack.insert(1500, { val: 123 });
		expect(
			stack.count({
				before: 50,
			})
		).toBe(0);
		expect(
			stack.count({
				before: 1250,
			})
		).toBe(3);
	});

	it('should filter after a timestamp', () => {
		const stack = new EventStack();
		stack.insert(1000, { val: 123 });
		stack.insert(1100, { val: 123 });
		stack.insert(1200, { val: 123 });
		stack.insert(1300, { val: 123 });
		stack.insert(1400, { val: 123 });
		stack.insert(1500, { val: 123 });
		expect(
			stack.count({
				after: 2000,
			})
		).toBe(0);
		expect(
			stack.count({
				after: 1350,
			})
		).toBe(2);
	});

	it('should filter recent events', () => {
		const stack = new EventStack();
		expect(
			stack.count({
				recent: MINUTE,
			})
		).toBe(0);
		stack.insert(1000, { val: 123 });
		stack.insert(2000, { val: 123 });
		stack.insert(3000, { val: 123 });
		stack.insert(Date.now() - MINUTE * 10, { val: 123 });
		stack.add({ val: 123 });
		expect(
			stack.count({
				recent: MINUTE,
			})
		).toBe(1);
		expect(
			stack.count({
				recent: HOUR,
			})
		).toBe(2);
	});

	it('should filter events after a certain event', async () => {
		const stack = new EventStack();

		// add events
		stack.add({ val: 1 });
		await sleep(0);
		stack.add({ val: 3 });
		await sleep(0);
		stack.add({ type: 'reset' });
		await sleep(0);
		stack.add({ val: 3 });
		await sleep(0);
		stack.add({ val: 3 });

		expect(
			stack.count({
				afterEvent: {
					whereAll: {
						type: {
							$eq: 'reset',
						},
					},
				},
			})
		).toBe(2);

		await sleep(0);
		stack.add({ type: 'reset' });

		expect(
			stack.count({
				afterEvent: {
					whereAll: {
						type: {
							$eq: 'reset',
						},
					},
				},
			})
		).toBe(0);
	});

	it('should group events by a property', () => {
		const stack = new EventStack();
		stack.add({ type: 'A', val: 5 });
		stack.add({ type: 'B', val: 6 });
		stack.add({ type: 'A', val: 7 });

		expect(
			stack.findGrouped({
				groupBy: 'type',
			}).A.length
		).toBe(2);
		expect(
			stack.findGrouped({
				groupBy: 'type',
			}).B.length
		).toBe(1);
	});

	it('should group events with a function', () => {
		const stack = new EventStack();
		stack.add({ type: 'A', val: 5 });
		stack.add({ type: 'B', val: 6 });
		stack.add({ type: 'A', val: 7 });

		expect(
			stack.findGrouped({
				group: ({ data }) => data.type.repeat(5),
			}).AAAAA.length
		).toBe(2);
		expect(
			stack.findGrouped({
				group: ({ data }) => data.type.repeat(5),
			}).BBBBB.length
		).toBe(1);
	});

	it('should count grouped events', () => {
		const stack = new EventStack();
		stack.add({ type: 'A', val: 5 });
		stack.add({ type: 'B', val: 6 });
		stack.add({ type: 'A', val: 7 });

		expect(
			stack.countGrouped({
				groupBy: 'type',
			})
		).toStrictEqual({
			A: 2,
			B: 1,
		});
	});
});
