import { satisfies } from '../src/satisfies';

describe('satisfies', () => {
	it('should check equality for strings', () => {
		expect(satisfies('abc', '$eq', 'abc')).toBe(true);
		expect(satisfies('abc', '$eq', 'def')).toBe(false);
	});

	it('should check equality for numbers', () => {
		expect(satisfies(123, '$eq', 123)).toBe(true);
		expect(satisfies(123, '$eq', 456)).toBe(false);
	});

	it('should check inequality for strings', () => {
		expect(satisfies('abc', '$neq', 'abc')).toBe(false);
		expect(satisfies('abc', '$neq', 'def')).toBe(true);
	});

	it('should check innequality for numbers', () => {
		expect(satisfies(123, '$neq', 123)).toBe(false);
		expect(satisfies(123, '$neq', 456)).toBe(true);
	});

	it('should check < for numbers', () => {
		expect(satisfies(5, '$lt', 4)).toBe(false);
		expect(satisfies(5, '$lt', 5)).toBe(false);
		expect(satisfies(5, '$lt', 6)).toBe(true);
	});
	it('should check <= for numbers', () => {
		expect(satisfies(5, '$lte', 4)).toBe(false);
		expect(satisfies(5, '$lte', 5)).toBe(true);
		expect(satisfies(5, '$lte', 6)).toBe(true);
	});
	it('should check > for numbers', () => {
		expect(satisfies(5, '$gt', 4)).toBe(true);
		expect(satisfies(5, '$gt', 5)).toBe(false);
		expect(satisfies(5, '$gt', 6)).toBe(false);
	});
	it('should check >= for numbers', () => {
		expect(satisfies(5, '$gte', 4)).toBe(true);
		expect(satisfies(5, '$gte', 5)).toBe(true);
		expect(satisfies(5, '$gte', 6)).toBe(false);
	});

	it('should only allow numbers for inequalities', () => {
		expect(() => satisfies('a', '$lt', 'b')).toThrowError();
		expect(() => satisfies('a', '$gt', 'b')).toThrowError();
		expect(() => satisfies('a', '$lte', 'b')).toThrowError();
		expect(() => satisfies('a', '$lte', 'b')).toThrowError();
	});

	it('should check "in" operator', () => {
		expect(satisfies('b', '$in', ['a', 'b', 'c'])).toBe(true);
		expect(satisfies('d', '$in', ['a', 'b', 'c'])).toBe(false);
	});
	it('should check "nin" operator', () => {
		expect(satisfies('b', '$nin', ['a', 'b', 'c'])).toBe(false);
		expect(satisfies('d', '$nin', ['a', 'b', 'c'])).toBe(true);
	});

	it('should only allow arrays for "in" operators', () => {
		expect(() => satisfies('b', '$in', 123)).toThrow();
		expect(() => satisfies('b', '$nin', 123)).toThrow();
		expect(() => satisfies('b', '$in', 'asdf')).toThrow();
		expect(() => satisfies('b', '$nin', 'asdf')).toThrow();
	});
});
